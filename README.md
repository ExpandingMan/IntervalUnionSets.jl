# IntervalUnionSets

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/IntervalUnionSets.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/IntervalUnionSets.jl/main?style=for-the-badge)](https://gitlab.com/ExpandingMan/IntervalUnionSets.jl/-/pipelines)

This package generalizes from [IntervalSets.jl](https://github.com/JuliaMath/IntervalSets.jl) to
define all 1-dimensional sets that can be expressed as a union of finitely many intervals, which
may or may not be disjoint.

