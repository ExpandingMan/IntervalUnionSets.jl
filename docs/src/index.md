```@meta
CurrentModule = IntervalUnionSets
```

# IntervalUnionSets

Documentation for [IntervalUnionSets](https://gitlab.com/ExpandingMan/IntervalUnionSets.jl).

```@index
```

```@autodocs
Modules = [IntervalUnionSets]
```
