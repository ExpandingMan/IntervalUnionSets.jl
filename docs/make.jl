using IntervalUnionSets
using Documenter

DocMeta.setdocmeta!(IntervalUnionSets, :DocTestSetup, :(using IntervalUnionSets); recursive=true)

makedocs(;
    modules=[IntervalUnionSets],
    authors="ExpandingMan <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/IntervalUnionSets.jl/blob/{commit}{path}#{line}",
    sitename="IntervalUnionSets.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/IntervalUnionSets.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
