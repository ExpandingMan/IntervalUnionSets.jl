#====================================================================================================
This more generic interval type is required to make IntervalUnion reasonably type stable.

We copy code from IntervalSets.Interval where possible
====================================================================================================#
struct GenericInterval{T} <: AbstractInterval{T}
    leftclosed::Bool
    rightclosed::Bool
    left::T
    right::T
end

function isclosedsym(o::Symbol)
    if o == :closed
        true
    elseif o == :open
        false
    else
        throw(ArgumentError("invalid open closed specifier: $o"))
    end
end

function GenericInterval(lo::Symbol, ro::Symbol, a, b)
    (a, b) = promote(a, b)
    GenericInterval(isclosedsym(lo), isclosedsym(ro), a, b)
end

# this is consistent with Interval
GenericInterval(a, b) = GenericInterval(:closed, :closed, a, b)

GenericInterval{T}(I::AbstractInterval) where {T} = GenericInterval{T}(isleftclosed(I), isrightclosed(I), endpoints(I)...)
GenericInterval(I::AbstractInterval) = GenericInterval{eltype(I)}(I)

_leftopenclosechar(closed::Bool) = closed ? '[' : '('
_rightopenclosechar(closed::Bool) = closed ? ']' : ')'

function Base.show(io::IO, m::MIME"text/plain", I::GenericInterval)
    print(io, _leftopenclosechar(isleftclosed(I)))
    show(io, leftendpoint(I))
    print(io, ", ")
    show(io, rightendpoint(I))
    print(io, _rightopenclosechar(isrightclosed(I)))
end

Base.convert(::Type{GenericInterval}, I::GenericInterval) = I
Base.convert(::Type{GenericInterval{T}}, I::AbstractInterval) where {T} = GenericInterval{T}(I)
Base.convert(::Type{GenericInterval}, I::AbstractInterval) = GenericInterval(I)

IntervalSets.endpoints(I::GenericInterval) = (I.left, I.right)

function GenericInterval(I::TypedEndpointsInterval{L,R,T}) where {L,R,T}
    GenericInterval(L, R, leftendpoint(I), rightendpoint(I))
end

IntervalSets.closedendpoints(I::GenericInterval) = (I.leftclosed, I.rightclosed)

function Base.:(∈)(v, I::GenericInterval)
    (lc, rc) = closedendpoints(I)
    (a, b) = endpoints(I)
    if lc && rc
        a ≤ v ≤ b
    elseif lc && !rc
        a ≤ v < b
    elseif !lc && rc
        a < v ≤ b
    else
        a < v < b
    end
end
Base.:(∈)(z::Complex, I::GenericInterval) = isreal(z) && real(z) ∈ I 
Base.:(∈)(::Missing, I::GenericInterval) = !isempty(I) && missing

function Base.isempty(I::GenericInterval)
    (lc, rc) = closedendpoints(I)
    (a, b) = endpoints(I)
    if lc && rc
        a > b
    else
        a ≥ b
    end
end

# need both signatures to avoid type piracy
for (Atype, Btype) ∈ [(:GenericInterval, :AbstractInterval), (:AbstractInterval, :GenericInterval),
                      (:GenericInterval, :GenericInterval),  # this one to fix ambiguity
                     ]
    @eval function Base.:(⊆)(A::$Atype, B::$Btype)
        isempty(A) && return true
        (Alc, Arc) = closedendpoints(A)
        (Blc, Brc) = closedendpoints(B)
        (Al, Ar) = endpoints(A)
        (Bl, Br) = endpoints(B)
        if Alc && !Blc
            (Bl < Al) && (Ar ≤ Br)
        elseif Arc && !Brc
            (Bl ≤ Al) && (Ar < Br)
        elseif Alc && Arc && !Blc && !Brc
            (Bl < Al) && (Ar < Br) 
        else
            (Bl ≤ Al) && (Ar ≤ Br)
        end
    end

    # we always return GenericInterval because it is most generic

    # assumes overlap
    @eval function _union(A::$Atype, B::$Btype)
        lc = if leftendpoint(A) == leftendpoint(B)
            isleftclosed(A) ? true : isleftclosed(B)
        elseif leftendpoint(A) < leftendpoint(B)
            isleftclosed(A)
        else
            isleftclosed(B)
        end
        rc = if rightendpoint(A) == rightendpoint(B)
            isrightclosed(A) ? true : isrightclosed(B)
        elseif rightendpoint(A) > rightendpoint(B)
            isrightclosed(A)
        else
            isrightclosed(B)
        end
        l = min(leftendpoint(A), leftendpoint(B))
        r = max(rightendpoint(A), rightendpoint(B))
        GenericInterval(lc, rc, l, r)
    end

    @eval function Base.:(∪)(A::$Atype, B::$Btype)
        T = promote_type(eltype(A), eltype(B))
        isempty(A) && return convert(GenericInterval, B)
        isempty(B) && return convert(GenericInterval, A)
        isuniondisjoint(A, B) && throw(ArgumentError("Cannot construct union of disjoint sets."))
        _union(A, B)
    end

    @eval function Base.:(∩)(A::$Atype, B::$Btype)
        (Al, Ar) = endpoints(A)
        (Bl, Br) = endpoints(B)
        (a, lc) = if Al > Bl
            (Al, isleftclosed(A))
        elseif Al == Bl
            (Al, !(isleftopen(A) || isleftopen(B)))
        else
            (Bl, isleftclosed(B))
        end
        (b, rc) = if Ar < Br
            (Ar, isrightclosed(A))
        elseif Ar == Br
            (Ar, !(isrightopen(A) || isrightopen(B)))
        else
            (Br, isrightclosed(B))
        end
        GenericInterval{promote_type(eltype(A),eltype(B))}(lc, rc, a, b)
    end
end

function Base.minimum(I::GenericInterval)
    isleftclosed(I) || throw(ArgumentError("$I is open on the left. Use infimum."))
    infimum(I)
end
function Base.maximum(I::GenericInterval)
    isrightclosed(I) || throw(ArgumentError("$I is open on the right. Use supremum."))
    supremum(I)
end

Base.Slice{T}(I::GenericInterval{U}) where {T<:AbstractUnitRange,U<:Integer} = Base.Slice{T}(minimum(I):maximum(I))
Base.Slice(I::GenericInterval{<:Integer}) = Base.Slice(minimum(I):maximum(i))
Base.UnitRange{T}(I::GenericInterval{<:Integer}) where {T} = UnitRange{T}(minimum(I), maximum(I))
Base.UnitRange(I::GenericInterval{T}) where {T<:Integer} = UnitRange{T}(I)
Base.range(I::GenericInterval{T}) where {T<:Integer} = UnitRange{T}(I)

function Base.clamp(I::GenericInterval)
    isclosedset(I) || throw(ArgumentError("can only clamp to closed sets"))
    clamp(t, leftendpoint(I), rightendpoint(I))
end

Random.gentype(::Type{GenericInterval{T}}) where {T} = float(T)
function Random.rand(rng::AbstractRNG, s::Random.SamplerTrivial{<:GenericInterval{T}}) where {T<:Real}
    I = s[]
    isclosedset(I) || throw(ArgumentError("can only randomly sample from closed sets"))
    isempty(I) && throw(ArgumentError("The interval should be non-empty"))
    (a, b) = endpoints(I)
    t = rand(rng, float(T))
    clamp(t*a + (1 - t)*b, I)
end

function Base.promote_rule(::Type{GenericInterval{T}}, ::Type{GenericInterval{U}}) where {T,U}
    GenericInterval{promote_type(T,U)}
end

function Base.float(I::GenericInterval{T}) where {T}
    GenericInterval{float(T)}(isleftclosed(I), isrightclose(I), endpoints(I)...)
end
