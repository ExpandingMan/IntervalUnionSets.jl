module IntervalUnionSets

using Random
using Reexport
@reexport using IntervalSets
using IntervalSets: Domain, TypedEndpointsInterval


"""
    isuniondisjoint(I1::AbstractInterval, I2::AbstractInterval)

Whether the union of `I1` and `I2` must be expressed as a union of disjoint intervals.  That is,
if this is false, it is possible to express the union as a single interval.
"""
function isuniondisjoint(d1::AbstractInterval, d2::AbstractInterval)
    # this is taken from IntervalSets, they don't provide this as a function
    !(any(∈(d1), endpoints(d2)) || any(∈(d2), endpoints(d1)))
end


include("genericinterval.jl")


abstract type AbstractIntervalUnion{T} <: Domain{T} end

Base.eltype(::Type{AbstractIntervalUnion{T}}) where {T} = T
Base.eltype(::Type{A}) where {A<:AbstractIntervalUnion} = eltype(supertype(A))

Base.convert(::Type{AbstractIntervalUnion}, A::AbstractIntervalUnion) = A
Base.convert(::Type{AbstractIntervalUnion{T}}, A::AbstractIntervalUnion{T}) where {T} = A

function standardizeintervals!(o::AbstractVector{GenericInterval{T}}, Is;
                               checksort::Bool=true
                              ) where {T}
    isempty(o) || empty!(o)
    isempty(Is) && return Vector{GenericInterval{T}}()
    if checksort && !issorted(Is, by=leftendpoint)
        throw(ArgumentError("argument to standardizeintervals! must be sorted by leftendpoint"))
    end
    I = Is[1]
    if length(Is) == 1
        push!(o, I)
        return o
    end
    for j ∈ 2:length(Is)
        if isempty(I)  # ignore empty subintervals
            I = Is[j]
        elseif isuniondisjoint(I, Is[j])
            push!(o, I)
            I = Is[j]
        else
            I = Base.:(∪)(I, Is[j])
        end
        j == length(Is) && push!(o, I)
    end
    o
end

function standardizeintervals(Is; kw...)
    T = (eltype ∘ eltype)(Is)
    standardizeintervals!(Vector{GenericInterval{T}}(), collect(Is); kw...)
end

struct IntervalUnion{T} <: AbstractIntervalUnion{T}
    intervals::Vector{GenericInterval{T}}

    function IntervalUnion{T}(Is::AbstractVector{GenericInterval{T}};
                              standardized::Bool=false,
                              do_sort::Bool=false,
                              checksort::Bool=!do_sort,
                             ) where {T}
        if !standardized
            do_sort && sort!(Is, by=leftendpoint)
            Is = standardizeintervals(Is; checksort)
        end
        new{T}(Is)
    end
end

function IntervalUnion(Is; standardized::Bool=false, do_sort::Bool=false, checksort::Bool=!do_sort)
    T = (eltype ∘ eltype)(Is)
    o = convert(Vector{GenericInterval{T}}, collect(Is))
    IntervalUnion{T}(o; standardized, do_sort, checksort)
end

IntervalUnion{T}() where {T} = IntervalUnion{T}(GenericInterval{T}[])

function Base.show(io::IO, m::MIME"text/plain", A::IntervalUnion)
    show(io, typeof(A))
    println(io, ":")
    if isempty(A)
        print(io, "  (empty)")
        return nothing
    end
    for j ∈ 1:nsubintervals(A)
        print(io, "  ")
        show(io, m, subinterval(A, j))
        j == nsubintervals(A) || print(io, "  ∪\n")
    end
end

# this only makes sense to use when A is in an invalid state
standardizeintervals!(A::IntervalUnion) = standardizeintervals!(A.intervals, copy(A.intervals))

nsubintervals(A::IntervalUnion) = length(A.intervals)
subinterval(A::IntervalUnion, j::Integer) = A.intervals[j]
firstsubinterval(A::IntervalUnion) = A.intervals[1]
lastsubinterval(A::IntervalUnion) = A.intervals[end]

# these are a bit dubious as a naming convention, but are consistent
for f ∈ (:leftendpoint, :isleftclosed, :isleftopen, :infimum)
    @eval IntervalSets.$f(A::IntervalUnion) = $f(firstsubinterval(A))
end
for f ∈ (:rightendpoint, :isrightclosed, :isrightopen, :supremum)
    @eval IntervalSets.$f(A::IntervalUnion) = $f(lastsubinterval(A))
end

Base.copy(A::IntervalUnion) = IntervalUnion(copy(A.intervals); standardized=true, do_sort=false, checksort=false)

IntervalSets.isopenset(A::IntervalUnion) = all(isopenset, A.intervals)
IntervalSets.isclosedset(A::IntervalUnion) = all(isclosedset, A.intervals)

# we should try to only allow one empty interval but this is valid in any case
IntervalSets.isempty(A::IntervalUnion) = all(isempty, A.intervals)

# this could be a lot more efficient in most cases...
function Base.union!(A::IntervalUnion, I::AbstractInterval)
    ivals = sort!([A.intervals; GenericInterval(I)], by=leftendpoint)
    standardizeintervals!(A.intervals, ivals; checksort=false)
    A
end

# we declare this must always be an IntervalUnion, not piracy
Base.:(∪)(A::IntervalUnion, I::AbstractInterval) = union!(copy(A), I) 
Base.:(∪)(I::AbstractInterval, A::IntervalUnion) = A ∪ I

# this is really inefficient...
Base.:(∪)(A::IntervalUnion, B::IntervalUnion) = IntervalUnion([A.intervals; B.intervals])

# we are defining our own ∪ which returns IntervalUnion for all AbstractInterval
(∪)(a...) = Base.:(∪)(a...)

(∪)(A::AbstractInterval, B::AbstractInterval) = IntervalUnion([A, B], do_sort=true) 

# we try not to make these too generic because of method ambiguities
Base.reduce(::typeof(∪), Is::AbstractArray{<:AbstractInterval}) = IntervalUnion(Is)
Base.reduce(::typeof(∪), Is::NTuple{N,AbstractInterval}) where {N} = IntervalUnion(Is)

function Base.intersect!(A::IntervalUnion, I::AbstractInterval)
    ivals = map!(iv -> I ∩ iv, A.intervals, A.intervals)
    println(ivals)
    standardizeintervals!(A)
    A
end

function Base.:(∩)(A::IntervalUnion, I::AbstractInterval)
    o = map(a -> a ∩ I, A.intervals)
    IntervalUnion(map(a -> a ∩ I, A.intervals), do_sort=false)
end
Base.:(∩)(I::AbstractInterval, A::IntervalUnion) = A ∩ I


export GenericInterval
export IntervalUnion

end
